﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalTCPChatClient
{
    public class TCPConversation
    {
        public string convID;
        public string[] users;
        public string convName;
        public ObservableCollection<TCPConversationMessage> messages;
        public int numberOfMessage;

        public TCPConversation()
        {
            messages = new ObservableCollection<TCPConversationMessage>();
            numberOfMessage = 0;
            convName = "Default name";
            convID = "";
            users = new string[0];
        }
    }
}
