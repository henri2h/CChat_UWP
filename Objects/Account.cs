﻿namespace UniversalTCPClientLibrary
{
    public class Account
    {
        public string accountName { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        // network
        public string host { get; set; }
        public int port { get; set; }
        public bool isWeb { get; set; }
        public string ID { get; set; }
        public bool IsDefault { get; set; }
    }
}