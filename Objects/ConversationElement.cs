﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalTCPChatClient.ObjectsHandeler
{
   public class ConversationElement
    {
        public String ConvName { get; set; }
        public String ConvID { get; set; }
    }
}
