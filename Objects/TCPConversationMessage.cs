﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalTCPChatClient
{
    public class TCPConversationMessage
    {

        public TCPConversationMessage() { }
        public TCPConversationMessage(string username, string convID)
        {
            this.Username = username;
            this.ConvID = convID;
            this.SendDate = DateTime.UtcNow.ToString();
            this.Content = "";
        }
        public string Username;
        public string ConvID;
        public int MessageID;

        public string SendDate;
        public dataType DataType;
        public string Content;

    }

    public class messageContent
    {
        public dataType DataType;
        // the content of the message or the path to the file
        public String content;
        public byte[] file;
    }
    public enum dataType
    {
        text,
        file,
        image
    }

}

