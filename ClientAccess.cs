﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniversalTCPChatClient;
using UniversalTCPChatClient.Objects;
using UniversalTCPChatClient.ObjectsHandeler;

namespace UniversalTCPClientLibrary
{
    /// <summary>
    /// CLient connector : bridge between the app and the client connector
    /// </summary>
    public class ClientAccess
    {
        bool clientCreated = false;
        public bool Created { get { return clientCreated; } }
        public ConversationsListManager ConvList = new ConversationsListManager();
        public ObservableCollection<ConversationElement> ConversationsList { get; set; }
        public ObservableCollection<TCPConversation> Conversation { get; set; }

        public String path = "";

        public IClient Client;

        public void CreateConversation(string[] v)
        {
            // create a new client action in the client action list
            throw new NotImplementedException();
        }



        /// <summary>
        /// Destroy the current clientConnector
        /// </summary>
        public void Destroy()
        {
            clientCreated = false;
            Core.AccountSelected = null;
        }


        // connection part
        /// <summary>
        /// Create a new client and try to update all informations
        /// </summary>
        /// <param name="serverAddress"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="isWeb"></param>
        public ClientAccess(Uri serverAddress, Account account, bool isWeb)
        {

            Core.AccountSelected = account;
            String userName = account.userName;
            String password = account.password;

            if (isWeb == true)
            {
                Client = new WebClient(userName, serverAddress);
            }
            else
            {
                Client = new TCPClient(serverAddress.Host, serverAddress.Port)
                {
                    Creating = true
                };
                Client.Connect();
            }

            System.Diagnostics.Debug.WriteLine("[ Librarie ] : connection method : " + Client.GetConnectionMethod().ToString());

            var a = TestConectionAsync();

            if (password == null) { password = ""; }
            Client.CurrentUser.username = userName;
            Client.CurrentUser.password = password;

            bool result = Client.LogIn();

            System.Diagnostics.Debug.WriteLine("[ Librarie ] : Client logged in : " + result);

            Client.Creating = false;
            Client.Created = true;

            clientCreated = true;
        }
        internal async Task<bool> TestConectionAsync()
        {
            bool connected = await Client.TestConnection();
            System.Diagnostics.Debug.WriteLine("Server is ok : " + connected + Environment.NewLine);
            return Client.Connected;
        }

        public Task SetConversationName(string text, string convID)
        {
            throw new NotImplementedException();
        }

        public async Task<string> GetConversationInformationsAsync(string convID)
        {
            if (ConvList.IsStored(convID))
            {
                return ConvList.GetConversationName(convID);
            }
            else if (Client.Connected)
            {
                String name = await Client.GetConversationName(convID);
                return name;
            }
            else
            {
                return "error";
            }
        }

        /// <summary>
        /// Return true if client is connected to server and ready to accept requests
        /// </summary>
        public bool ClientIsConnected
        {
            get { return Client.Connected; }
        }


        public Account _Account { get; set; }

        public List<ConversationElement> GetConversationList()
        {
            return ConvList.ConversationList;
        }

        public void GetConversationMessages(String ConvID)
        {
        }

        async void UpdateAllAsync()
        {
            await UpdateConversationListAsync();


            foreach (ConversationElement Conv in ConversationsList)
            {
                bool success = await UpdateConversationAsync(Conv.ConvID);
            }

        }


        /// <summary>
        /// Update conversation list, return true if a new message has been recieved
        /// </summary>
        /// <param name="convID"></param>
        /// <returns></returns>
        public async Task<bool> UpdateConversationAsync(String convID)
        {
            try
            {
                TCPConversation conv = await Client.GetConversation(convID);
                TCPConversation oldConv = ConversationManager.GetConversation(convID);

                if (oldConv.messages.Count != conv.messages.Count)
                {
                    // we have received a new message
                    ConversationManager.SaveConversation(conv, convID);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Update conversation list (fetch list from server)
        /// </summary>
        public async Task<bool> ForceUpdateConversationListAsync()
        {
            return await UpdateConversationListAsync();
        }

        public async Task<bool> UpdateConversationListAsync()
        {
            try
            {
                ConversationElement[] convs = await Client.ListConversations();
                ConvList.Clear();
                foreach (ConversationElement convItem in convs)
                {
                    ConvList.Add(convItem);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public async void SendConversationMessage(TCPConversationMessage tcpConvMess)
        {
            await Client.SendConversationMessage(tcpConvMess);
        }

        /// <summary>
        /// Return the specified conversation, if doesn't exist, return an empty TCPConversation object
        /// </summary>
        /// <param name="convID"></param>
        /// <returns></returns>
        public TCPConversation GetConversation(string convID)
        {
            return ConversationManager.GetConversation(convID);
        }

        public string GetFilePath(string fileName, string convID)
        {
            string path = Core.localFolder.Path;
            path = Path.Combine(path, Core.AccountSelected.ID, "conversations", convID);

            return Path.Combine(path, fileName);
        }
        // to implement
        public bool FileAlreadyDownloaded(string fileName, string convID)
        {
            return File.Exists(GetFilePath(fileName, convID));
        }

        public async Task<byte[]> GetFileAsync(string fileName, string convID) { return await GetFileAsync(fileName, convID, 1); }
        public async Task<byte[]> GetFileAsync(string fileName, string convID, int dMode)
        {
            string path = Core.localFolder.Path;
            path = Path.Combine(path, Core.AccountSelected.ID, "conversations", convID);

            string filePath = Path.Combine(path, fileName);

            byte[] imageBytes;
            if (FileAlreadyDownloaded(fileName, convID) == false || dMode == 2)
            {
                if (dMode > 0)
                {
                    imageBytes = await Client.GetConversationImage(fileName, convID);
                    Directory.CreateDirectory(path);
                    File.WriteAllBytes(filePath, imageBytes);

                    System.Diagnostics.Debug.WriteLine("New image downloaded : " + fileName + ", convID : " + convID);
                }
                else return null;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("File : " + fileName + " already exist");
                imageBytes = File.ReadAllBytes(filePath);
            }

            return imageBytes;
        }

        internal void SendConversationMessage(messageContent message, string convID)
        {
            TCPConversationMessage mess = new TCPConversationMessage();
            mess.Content = message.content;
            mess.DataType = message.DataType;
            mess.ConvID = convID;
            SendConversationMessage(mess);
        }
    }
}
