﻿using Microsoft.AppCenter;
using Microsoft.AppCenter.Crashes;
using UniversalTCPChatClient.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;


namespace UniversalTCPChatClient
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {

            this.InitializeComponent();

            System.Diagnostics.Debug.WriteLine("App data location : " + Core.localFolder.Path);

            ChangeFrame();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ChangeFrame();
        }

        bool ChangeFrame()
        {
            try
            {
                Frame rootFrame = Window.Current.Content as Frame;
                bool canChange = rootFrame.Navigate(typeof(AccountPage));
                return canChange;
            }
            catch (System.Exception ex)
            {
                Crashes.TrackError(ex);
                return false;
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            ChangeFrame();
        }
    }
}
