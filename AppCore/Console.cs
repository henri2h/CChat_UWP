﻿using System;

namespace UniversalTCPChatClient
{
    internal class Console
    {
        internal static void WriteLine(string v)
        {
            System.Diagnostics.Debug.WriteLine(v);
        }

        internal static void Write(string v)
        {
            
            System.Diagnostics.Debug.Write(v);
        }
    }
}