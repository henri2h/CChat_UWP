﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.IO;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using UniversalTCPChatClient.UI.ConversationView.Detail;
using UniversalTCPChatClient.UI.ConversationView.conversationElements;
using Microsoft.AppCenter.Analytics;
using UniversalTCPClientLibrary;
using Microsoft.AppCenter.Crashes;

namespace UniversalTCPChatClient
{
    public class Core
    {
        public static bool clientExecutorCreated = false;
        public static ClientAccess Client { get; set; }

        public static Windows.Storage.StorageFolder localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;

        public static string convIDIn = "";
        public static Account AccountSelected
        {
            get { return accountSelected; }
            set
            {
                // set another client
                if (Client != null) Client.Destroy();
                accountSelected = value;
            }
        }

        const string accountFileName = "accounts";

        public static List<Account> accountsList = new List<Account>();
        static Account accountSelected;
        internal static string selectedFileName;
        internal static Frame frame;

        public static void SaveAccountList()
        {
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(accountsList);
            string filePath = Path.Combine(localFolder.Path, accountFileName);
            File.WriteAllText(filePath, json);
        }

        public static async Task LoadAccountList()
        {
            try
            {
                IStorageItem accountFile = await localFolder.TryGetItemAsync(accountFileName);
                if (accountFile != null) // if file exist
                {
                    string json = await FileIO.ReadTextAsync(accountFile as StorageFile);
                    accountsList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Account>>(json);
                }
                else
                {
                    accountsList = new List<Account>();
                    Account acc = new Account();
                    acc.accountName = "File not exist";
                    acc.isWeb = true; ;
                    acc.host = "henri2h.fr";
                    acc.port = 8443;
                    acc.userName = "quentin";
                }
            }
            catch (Exception ex)
            {
                ex.Source = "Core.LoadAccountList";
                Crashes.TrackError(ex);
                // because the file didn't exist;
                accountsList = new List<Account>();
            }
        }

        public static bool CreateClient()
        {
            Client = null;

            UriBuilder uri = new UriBuilder()
            {
                Host = AccountSelected.host,
                Port = AccountSelected.port,
                Scheme = "https" // warning : we should be a
            };

            Client = new ClientAccess(uri.Uri, AccountSelected, AccountSelected.isWeb);

            //System.Diagnostics.Debug.WriteLine("Server is ok : " + Client.TestConnection() + Environment.NewLine);
            // System.Diagnostics.Debug.WriteLine("connection method : " + Client.GetConnectionMethod().ToString());

            System.Diagnostics.Debug.WriteLine("[ Core ] : Client logged in : " + Client.ClientIsConnected);
            Analytics.TrackEvent("Client logged in : " + Client.ClientIsConnected);
            return Client.ClientIsConnected;
        }

        internal static void DisplayImage(object sender, RoutedEventArgs e)
        {
            Button bt = (Button)sender;
            ImageDisplay imgDisp = (ImageDisplay)bt.Content;
            selectedFileName = imgDisp.FileName;
            frame.Navigate(typeof(ImageViewer));
        }

        public static void writeDebugLine(string objectName, string text)
        {
            System.Diagnostics.Debug.WriteLine("[" + objectName + " : " + DateTime.Now.ToString() + " ] : " + text);
            Analytics.TrackEvent("[" + objectName + " ] : " + text);
        }



    }
}
