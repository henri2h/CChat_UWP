﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalTCPChatClient.ObjectsHandeler
{
    class FilesManager
    {
        private static String AccountID { get => Core.AccountSelected.ID; set => Core.AccountSelected.ID = value; }

        private static string GetPath(String element, String FileName)
        {
            string path = Core.localFolder.Path;
            path = Path.Combine(path, AccountID, element);
            Directory.CreateDirectory(path);

            string filePath = Path.Combine(path, FileName);
            return filePath;
        }

        private static bool AreDifferent(object conv, object local)
        {
            if (conv != local) return true;
            return false;
        }
    }
}

