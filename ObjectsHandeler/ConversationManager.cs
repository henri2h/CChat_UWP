﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalTCPChatClient.ObjectsHandeler
{
    class ConversationManager
    {
        private static bool AreDifferent(TCPConversation conv)
        {
            TCPConversation local = GetConversation(conv.convID);
            if (conv != local) return true;
            return false;
        }

        private static string GetPath(string convID)
        {
            string path = Core.localFolder.Path;
            path = Path.Combine(path, Core.AccountSelected.ID, "conversations", convID);
            Directory.CreateDirectory(path);
            string filePath = Path.Combine(path, "messages");
            return filePath;
        }
        
        public static void SaveConversation(TCPConversation conv, string convID)
        {
            string path = GetPath(convID);
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(conv);
            File.WriteAllText(path, json);
        }

        public static TCPConversation GetConversation(string convID)
        {
            string path = GetPath(convID);
            if (File.Exists(path))
            {
                string json = File.ReadAllText(path);
                TCPConversation conv = Newtonsoft.Json.JsonConvert.DeserializeObject<TCPConversation>(json);
                return conv;
            }
            else return new TCPConversation();
        }

        public static bool IsTheConversationSaved(string convID)
        {
            string path = GetPath(convID);
            return File.Exists(path);
        }

       
    }
}

