﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalTCPChatClient.ObjectsHandeler
{
    /// <summary>
    /// A class to store and retrive the conversation list
    /// TODO : Add a way to store those data in a database for easier access (faster ????)
    /// Major rewrite to do
    /// TODO : see if using an observable collection would be better
    /// </summary>
    public class ConversationsListManager
    {
        /// <summary>
        /// cvl : local variable to store temporarly (before saving) the conversation list for faster retrieval
        /// </summary>
        List<ConversationElement> cvl;

        /// <summary>
        /// Set the conversation list and save it
        /// </summary>
        /// <param name="list"></param>
        public void SetConverationList(List<ConversationElement> list)
        {
            cvl = list;
            SaveConversationList();
        }

        /// <summary>
        /// Add an item to the converation list and save it
        /// </summary>
        /// <param name="conversationElement"></param>
        internal void Add(ConversationElement conversationElement)
        {
            LoadList(); // List should be loaded first
            cvl.Add(conversationElement);
            SaveConversationList();
        }

        internal void Clear()
        {
            cvl.Clear();
        }

        bool loaded = false; // to store the status
        /// <summary>
        /// load list from storage if not loaded else, do nothing
        /// </summary>
        internal void LoadList()
        {
            if (loaded == false)
            {
                cvl = GetConversationsList();
                loaded = true;
            }
        }

        // store the conversation list, load if needed and save is a change is made
        /// <summary>
        /// Store and retrieve the conversation list
        /// </summary>
        public List<ConversationElement> ConversationList {
            get {
                LoadList();
                return cvl;
            }
        }

        private bool AreDifferent(List<ConversationElement> list)
        {
            if (list != ConversationList) return true;
            return false;
        }

        private static string GetPath()
        {
            string path = Core.localFolder.Path;
            path = Path.Combine(path, Core.AccountSelected.ID, "conversations");
            Directory.CreateDirectory(path);
            string filePath = Path.Combine(path, "conversationlist");
            return filePath;
        }

        
        void SaveConversationList()
        {
            string path = GetPath();
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(ConversationList);
            File.WriteAllText(path, json);
        }

        /// <summary>
        /// Load conversation list from storage
        /// </summary>
        /// <returns></returns>
        List<ConversationElement> GetConversationsList()
        {
            string path = GetPath();
            if (File.Exists(path))
            {
                string json = File.ReadAllText(path);
                List<ConversationElement> list = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ConversationElement>>(json);
                return list;
            }
            List<ConversationElement> items = new List<ConversationElement>();
            return items;

        }

        public static bool IsTheConversationListSaved()
        {
            try
            {
                string path = GetPath();
                return File.Exists(path);
            }
            catch { return false; }
        }

        /// <summary>
        /// Check if the conversation with this name is stored
        /// </summary>
        /// <param name="convID"></param>
        /// <returns></returns>
        internal bool IsStored(string convID)
        {
          foreach (ConversationElement cv in ConversationList)
            {
                if(cv.ConvID == convID)
                {
                    return true;
                }
            }
            return false;
        }

        internal string GetConversationName(string convID)
        {
            throw new NotImplementedException();
        }

       
    }
}