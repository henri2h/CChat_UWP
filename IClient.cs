﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UniversalTCPChatClient.Objects;
using UniversalTCPChatClient.ObjectsHandeler;
using UniversalTCPChatClient.TCPCommunication.Client;

namespace UniversalTCPChatClient
{
    public interface IClient
    {
        // Globals
        string ProtocolVersion { get; }

        User CurrentUser { get; }
        bool Connected { get; }
        bool Created { get; set; }

        bool LogedIn { get; }
        bool Creating { get; set; }


        ClientCommunication Client { get; }

        // TCP Methods
        void Connect();
        bool CreateConnection();
        Task<bool> TestConnection();

        // Globals
        ConnectionMethod GetConnectionMethod();

        Task<ConversationElement[]> ListConversations();
        Task<string> GetConversationName(string name);
        bool LogIn();

        bool IsClientLogedIn { get; }

        
        int ToRead { get; }


        Task SendConversationMessage(string name, string convID);
        Task SendConversationMessage(TCPConversationMessage tcpConvMess);

        Task<byte[]> GetConversationImage(string fileName, string convID);
        Task CreateConversation(string[] users);
        Task<TCPConversation> GetConversation(string convID);

        // specific to TCP connection
        bool SendMessage(string text);
        string[] ReadMessageFromServer(int number);
        Task SetConversationName(string text, string convID);
    }
}
