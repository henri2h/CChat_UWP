﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniversalTCPChatClient.Objects;
using UniversalTCPChatClient.ObjectsHandeler;
using UniversalTCPChatClient.TCPCommunication.Client;

namespace UniversalTCPChatClient
{
    public class WebClient : IClient
    {
        private HttpClient client;
        private string token;
        public User currentUser = new User();
        // not used
        public bool created = true;

        // this has to be dynamicaly set
        public bool logedIn = true;
        public bool creating = false;
        public bool isClientLogedIn;



        string IClient.ProtocolVersion { get { return "1.1.0"; } }
        User IClient.CurrentUser { get { return currentUser; } }
        bool IClient.Connected { get { return isClientLogedIn; } }
        bool IClient.Created { get { return created; } set { created = value; } }
        bool IClient.LogedIn { get { return logedIn; } }
        bool IClient.Creating { get { return creating; } set { creating = value; } }

        ClientCommunication IClient.Client { get { return null; } }

        // not used in the web client
        int IClient.ToRead { get { return 0; } }

        bool IClient.IsClientLogedIn
        {
            get { return isClientLogedIn; }
        }

        public WebClient(string username, Uri baseUri)
        {
            currentUser = new User();
            this.client = new HttpClient(username, "", baseUri);
            try
            {
                //TODO : check if we are connected to the server
                isClientLogedIn = true;
                // maybe client is connected
            }
            catch
            {
                isClientLogedIn = false;
            }
        }


        void IClient.Connect()
        {
            System.Diagnostics.Debug.WriteLine("Un usefull in Http client");
        }

        bool IClient.CreateConnection()
        {
            System.Diagnostics.Debug.WriteLine("Un usefull in Http client");
            return true;
        }

        async Task IClient.CreateConversation(string[] users)
        {
            string usersJSON = Newtonsoft.Json.JsonConvert.SerializeObject(users);
            await client.POSTrequestStringAsync("/Conversation/CreateConversation", usersJSON);
        }


        async Task<TCPConversation> IClient.GetConversation(string convID)
        {
            dynamic dynamicJson = new ExpandoObject();
            dynamicJson.ConvID = convID;
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(dynamicJson);
            string conversation = await client.POSTrequestStringAsync("/Conversation/GetConversation", json);
            return Newtonsoft.Json.JsonConvert.DeserializeObject<TCPConversation>(conversation);
        }

        async Task<byte[]> IClient.GetConversationImage(string fileName, string convID)
        {

            dynamic dynamicJson = new ExpandoObject();
            dynamicJson.convID = convID;
            dynamicJson.fileName = fileName;

            string json = Newtonsoft.Json.JsonConvert.SerializeObject(dynamicJson);
            byte[] data = await client.POSTrequestBytesAsync("/Conversation/GetPicture", json);
            return data;

        }

        async Task<string> IClient.GetConversationName(string name)
        {
            dynamic dynamicJson = new ExpandoObject();
            dynamicJson.convID = name;
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(dynamicJson);
            string text = await client.POSTrequestStringAsync("/Conversation/GetConversationName", json);
            return text;
        }
        async Task IClient.SetConversationName(string convName, string convID)
        {
            dynamic dynamicJson = new ExpandoObject();
            dynamicJson.convID = convID;
            dynamicJson.convName = convName;
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(dynamicJson);
            string text = await client.POSTrequestStringAsync("/Conversation/SetConversationName", json);
            if (text != "OK") { throw new Exception("Conversation name not changed"); }
        }

        async Task<ConversationElement[]> IClient.ListConversations()
        {
            string text = await client.POSTrequestStringAsync("/Conversation/ListConversations", "");
            ConversationElement[] convIDS = Newtonsoft.Json.JsonConvert.DeserializeObject<ConversationElement[]>(text);
            return convIDS;
        }

        async Task IClient.SendConversationMessage(TCPConversationMessage nMess)
        {
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(nMess);
            string success = await client.POSTrequestStringAsync("/Conversation/SendConversationMessage", json);
            if (success != "OK") { throw new Exception("Message not sended"); }
        }

        async Task IClient.SendConversationMessage(string text, string convID)
        {
            TCPConversationMessage nMess = new TCPConversationMessage(currentUser.username, convID);
            
            nMess.Content = text;
            nMess.DataType = dataType.text;
            nMess.SendDate = DateTime.Now.ToString();

            string toSend = Newtonsoft.Json.JsonConvert.SerializeObject(nMess);
            await client.POSTrequestStringAsync("/Conversation/Conversation/SendConversationMessage", toSend);
        }

        // to implement
        bool IClient.LogIn()
        {
            return true;
        }

        async Task<bool> IClient.TestConnection()
        {
            try
            {
                string success = await client.POSTrequestStringAsync("/isUp", "");
                if (success == "OK")
                {
                    return true;
                }
                else if(success == "KO")
                {
                    return false;
                }
                else
                {
                    // Here is an issue, invalid response
                    System.Diagnostics.Debug.WriteLine("Could not connect");
                }
                return false;
            }

            catch
            {
                return false;
            }
        }


        ConnectionMethod IClient.GetConnectionMethod()
        {
            return ConnectionMethod.password;
        }


        // unused methods
        string[] IClient.ReadMessageFromServer(int number)
        {
            throw new NotImplementedException();
        }
        bool IClient.SendMessage(string text)
        {
            throw new NotImplementedException();
        }
    }
}
