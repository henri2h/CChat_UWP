﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UniversalTCPChatClient.Objects;
using UniversalTCPChatClient.ObjectsHandeler;
using UniversalTCPChatClient.TCPCommunication.Client;

namespace UniversalTCPChatClient
{
    public class TCPClient : IClient
    {

        public User currentUser = new User();
        public bool connected = false;
        public bool created = false;

        public bool logedIn = false;
        public bool creating = false;
        public bool isClientLogedIn;

        TCPCommunication.Client.ClientCommunication client;

        string IClient.ProtocolVersion { get { return "1.1.0"; } }
        User IClient.CurrentUser { get { return currentUser; } }
        bool IClient.Connected { get { return connected; } }
        bool IClient.Created { get { return created; } set { created = value; } }
        bool IClient.LogedIn { get { return logedIn; } }
        bool IClient.Creating { get { return creating; } set { creating = value; } }


        ClientCommunication IClient.Client { get { return client; } }


        public TCPClient(string ip, int inputPort)
        {
            currentUser = new User();
            creating = true;
            int port = inputPort;
            client = new TCPCommunication.Client.ClientCommunication(ip, port);
        }

        void IClient.Connect()
        {

            creating = true;
            connected = client.connect();
            //  client.AutoFlush = true;
            System.Diagnostics.Debug.WriteLine("started");
            client.ReceiveTimeout = 500;
            creating = false;
        }
        bool IClient.CreateConnection()
        {
            client.WriteCommand("connection");
            string response = client.readCommand();

            if (response == "connectionOK")
            {
                System.Diagnostics.Debug.WriteLine("Connected to the server");
                return true;
            }
            else if (response == "connectionKO")
            {
                System.Diagnostics.Debug.WriteLine("Error, connection not acceted");
                return false;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Error, server didn't respond");
                return false;
            }

        }

        ConnectionMethod IClient.GetConnectionMethod()
        {
            client.WriteCommand("connectionMethod");
            string response = client.readCommand();
            if (response == "anonymous")
            {
                return ConnectionMethod.anonymous;
            }
            else if (response == "password")
            {
                return ConnectionMethod.password;
            }

            throw new NotImplementedException("Not the good method");
            //return ConnectionMethod.anonymous;

        }

        async Task<string> IClient.GetConversationName(string name)
        {
            client.WriteCommand("sendConversationName");
            if (client.readCommand() == "OK")
            {
                client.WriteLine(name);
                return client.readLine();
            }
            throw new Exception("Server didn't respond properly in getConversationName");

        }

        bool IClient.LogIn()
        {
            client.WriteCommand("connect");

            string response = client.readCommand();
            if (response == "connectOK")
            {
                // now the client should send the informations
                client.WriteLine(currentUser.username);
                client.WriteLine(currentUser.password);
                string loginResponse = client.readCommand();

                if (loginResponse == "connectSuccess")
                {
                    // client connected
                    System.Diagnostics.Debug.WriteLine("Client connected");
                    logedIn = true;
                    return true;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("Client failed to connect : " + loginResponse);
                    return false;
                }
            }
            else if (response == "connectKO")
            {
                System.Diagnostics.Debug.WriteLine("Connection refuse by the server, retry");
                return false;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("error in the connect method" + Environment.NewLine + "Server didn't respond correctly : " + response);
                return false;
            }
        }

        bool IClient.IsClientLogedIn
        {
            get
            {

                client.WriteCommand("isLoggedIn");
                string response = client.readCommand();
                if (response == "connectedOK")
                {
                    return true;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("the user isn't connected, response : " + response);
                    return false;
                }

            }
        }

        async Task IClient.SendConversationMessage(TCPConversationMessage nMess)
        {
            client.WriteCommand("sendMessageToConversation");
            client.readCommand();
            
            string toSend = Newtonsoft.Json.JsonConvert.SerializeObject(nMess);
            client.WriteLine(toSend);
        }
        async Task IClient.SendConversationMessage(string text, string convID)
        {
            client.WriteCommand("sendMessageToConversation");
            client.readCommand();

            TCPConversationMessage nMess = new TCPConversationMessage(currentUser.username, convID);

            nMess.Content =  text;
            nMess.DataType = dataType.text;

            nMess.SendDate = DateTime.Now.ToString();

            string toSend = Newtonsoft.Json.JsonConvert.SerializeObject(nMess);
            client.WriteLine(toSend);
        }



        int IClient.ToRead
        {
            get
            {
                if (isClientLogedIn)
                {
                    client.WriteCommand("toRead");
                    int num = Convert.ToInt32(client.readLine());

                    return num;
                }

                return 0;
            }
        }

        public bool Creating { get; internal set; }

        async Task<ConversationElement[]> IClient.ListConversations()
        {
            client.WriteCommand("listConversations");
            client.readCommand();
            string text = client.readLine();
            ConversationElement[] convIDS = Newtonsoft.Json.JsonConvert.DeserializeObject<ConversationElement[]>(text);

            return convIDS;

        }

        async Task<byte[]> IClient.GetConversationImage(string fileName, string convID)
        {
            client.WriteCommand("getPictureFromConversation");
            string response = client.readCommand();
            if (response != "OK")
            {
                System.Diagnostics.Debug.WriteLine("Error in getting the image : reciving " + response);
                throw new Exception("Error in downloading the image : " + response);
            }

            client.WriteLine(convID);
            client.WriteLine(fileName);

            if (client.readCommand() == "OK")
            {
                byte[] data = client.ReadDataWithLenght();
                if (data == null) { throw new Exception("Data is null"); }
                return data;
            }
            else { throw new Exception("Can't read"); }

            string text = client.scanRead();
            if (text != null)
            {
                System.Diagnostics.Debug.WriteLine("[Debug] : still remain lines : {" + text + "}");
            }

        }
        async Task IClient.CreateConversation(string[] users)
        {
            client.WriteCommand("createConversation");
            client.readCommand();
            string usersJSON = Newtonsoft.Json.JsonConvert.SerializeObject(users);
            client.WriteLine(usersJSON);

        }
        async Task<TCPConversation> IClient.GetConversation(string convID)
        {
            client.WriteCommand("sendConversation");
            string result = client.readCommand();
            if (result == "OK")
            {
                client.WriteLine(convID);
                string conversations = client.readStringWithLenght();
                return Newtonsoft.Json.JsonConvert.DeserializeObject<TCPConversation>(conversations);
            }
            else { throw new NotImplementedException(); }
        }



        string[] IClient.ReadMessageFromServer(int number)
        {
            System.Diagnostics.Debug.WriteLine("Reading the messages : ");
            List<string> messages = new List<string>();
            client.WriteCommand("readMessageFromServer");
            client.WriteLine(number.ToString());
            for (int i = 0; i < number; i++)
            {
                string mess = client.readLine();
                System.Diagnostics.Debug.WriteLine("Message : " + i + " : " + mess);
                messages.Add(mess);
            }
            return messages.ToArray();
        }


        bool IClient.SendMessage(string text)
        {
            // creating the message
            Message mess = new Message()
            {
                message = text,
                username = currentUser.username,
                date = DateTime.Now.ToUniversalTime().ToString()
            };
            string message = Newtonsoft.Json.JsonConvert.SerializeObject(mess);
            System.Diagnostics.Debug.WriteLine("message : " + message);

            client.WriteCommand("JSONMessage");
            string response = client.readCommand();

            if (response == "OK")
            {
                client.WriteLine(message);

                return true;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Could not send the message");

                return false;
            }

        }

        public Task SetConversationName(string text, string convID)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> TestConnection()
        {
            client.WriteCommand("connection");
            string response = client.readCommand();
            if (response == "connectionOK")
            {
                System.Diagnostics.Debug.WriteLine("Server respond correctly");

                return true;
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Server failed to respond correctly");

                return false;
            }
        }
    }
}

