﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalTCPChatClient.Client
{
    public class CommandError : Exception
    {
        public CommandError(CommandErrors reason) : base(String.Format("Unexpected error hapend in the client : {0}", reason)) { }
        public enum CommandErrors
        {
            NotACommand,
            IsNull
        }
    }
}
