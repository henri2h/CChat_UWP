﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalTCPChatClient.Client
{
    public class ConnectionError : System.Exception
    {
        public ConnectionError(ConnectionErrorReason reason) : base(String.Format("Unexpected error hapend in TCP Client Communication : {0}", reason)) { }
    }
    public enum ConnectionErrorReason
    {
        ServerNotResponding,
        ConnectionRefused,
        InvalidResponse
    }
}
