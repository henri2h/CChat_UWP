﻿using Microsoft.AppCenter.Crashes;
using System;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using UniversalTCPChatClient.Client;
using Windows.Networking;
using Windows.Networking.Sockets;

namespace UniversalTCPChatClient.TCPCommunication.Client
{
    public class ClientCommunication
    {
        public StreamSocket socket;
        public int ReceiveTimeout = 500;


        public string Available;
        public bool readConnectionResponse = true;

        public StreamReader sr;
        public StreamWriter sw;

        HostName hostName;
        int port;


        public ClientCommunication(string ip, int portIn)
        {
            hostName = new HostName(ip);
            port = portIn;
            socket = new StreamSocket();
            socket.Control.NoDelay = false;
        }

        public bool connect()
        {
            try
            {
                // Connect to the server
                Task.Run(async () => { await socket.ConnectAsync(hostName, port.ToString()); }).Wait();

                sr = new StreamReader(socket.InputStream.AsStreamForRead());
                sw = new StreamWriter(socket.OutputStream.AsStreamForWrite());

                sw.AutoFlush = true;

                //only if we connect to a server wich respond "@OK" when the client connect
                if (readConnectionResponse)
                {
                    string inputOk = readLine();
                    if (inputOk != "@OK")
                    {
                        System.Diagnostics.Debug.WriteLine("Connection error");
                        throw new ConnectionError(ConnectionErrorReason.InvalidResponse);

                    }
                    else
                    {
                        System.Diagnostics.Debug.WriteLine("Connected");
                        return true;
                    }
                }

            }
            catch (Exception exception)
            {

                exception.Source = "ClientCommunication.creation";
                Crashes.TrackError(exception);
                throw;

            }
            return false;
        }


        // read
        public string readCommand()
        {
            string command = this.readLine();
            if (command != null)
            {
                if (command[0] == '@') { return command.Remove(0, 1); }
                else { throw new CommandError(CommandError.CommandErrors.NotACommand); }
            }

            System.Diagnostics.Debug.WriteLine("error on recieving command : " + command);
            return command;
        }

        public string readLine() { return sr.ReadLine(); }
        public string readStringWithLenght()
        {
            UTF8Encoding encod = new UTF8Encoding();
            string msg = encod.GetString(ReadDataWithLenght());
            return msg;
        }
        public byte[] ReadDataWithLenght()
        {
            string dataLenght = readLine();
            int msgLenght = int.Parse(dataLenght);

            //  byte[] buffer = ReadNBytes(4);

            //  int msgLenght = BitConverter.ToInt32(buffer, 0);
            if (msgLenght == 0) throw new Exception("Value cannot be equal to 0");
            System.Diagnostics.Debug.WriteLine("Going to read : " + msgLenght + " bytes");

            byte[] buffer = ReadNBytes(msgLenght);

            WriteCommand("RECIVED");
            string readed = readCommand();
            if (readed == "SENDED_OK") { return buffer; }
            else { throw new Exception("Not reciving good data"); }

        }

        public byte[] ReadNBytes(int lenght)
        {
            byte[] buffer = new byte[lenght];
            int bytesRead = 0;
            int chunk;
            bool exec = false;
            while (bytesRead < lenght)
            {
                if (sr.EndOfStream) { System.Diagnostics.Debug.WriteLine("Reached the end of stream"); }

                chunk = sr.BaseStream.Read(buffer, bytesRead, buffer.Length - bytesRead);
                if (chunk == 0) { throw new Exception("Unespected disconnect"); }
                bytesRead += chunk;

                if (exec)
                {
                    System.Diagnostics.Debug.WriteLine("Already used");
                }
                else exec = true;

            }
            return buffer;
        }
        public void WriteDataWithLenght(byte[] data)
        {
            byte[] intBytes = BitConverter.GetBytes(data.Length);
            if (intBytes.Length != 4) { System.Diagnostics.Debug.WriteLine("Size : " + intBytes.Length); }
            sw.BaseStream.Write(intBytes, 0, intBytes.Length);
            sw.BaseStream.Write(data, 0, data.Length);
            sw.BaseStream.Flush();
        }


        // write
        public void Write(string value) { sw.Write(value); }
        public void WriteCommand(string value) { this.WriteLine("@" + value); }
        public void WriteLine(string value)
        {
            if (value == null) { System.Diagnostics.Debug.WriteLine("Error, sending null"); throw new NullReferenceException(); }
            sw.WriteLine(value);
        }
        public string scanRead()
        {
            if (sr.EndOfStream == false)
            {
                return sr.ReadToEnd();
            }
            else { return null; }
        }



        public void Close()
        {
            sr.Dispose();
            socket.Dispose();
        }

    }

}
