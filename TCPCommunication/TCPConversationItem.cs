﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniversalTCPChatClient.TCPCommunication
{
    public class TCPConversationItem
    {
        public string convID { get; set; }
        public bool hasImage { get; set; }
        public string[] users { get; set; }
    }
}
