﻿using System;
using System.Collections.Generic;
using UniversalTCPClientLibrary;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace UniversalTCPChatClient.UI.AccountView
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class CreateConversation : Page
    {
        public CreateConversation()
        {
            this.InitializeComponent();
        }

        public List<String> users = new List<string>();
        public void addConversation()
        {
            users.Add(Core.Client._Account.userName);
            if (users.ToArray().Length > 0)
            {
                Core.Client.CreateConversation(users.ToArray());
            }

            users = new List<string>();
        } 


        private void UIClearUser_Click(object sender, RoutedEventArgs e)
        {
            users = new List<string>();
            UIStackUsers.Children.Clear();
        }

        private void UIAddUser_Click(object sender, RoutedEventArgs e)
        {
            users.Add(UITbName.Text);

            TextBlock tb = new TextBlock();
            tb.Text = UITbName.Text;
            UIStackUsers.Children.Add(tb);

            // clear the UI
            UITbName.Text = "";

        }


        private void UITBCreateConversation_Click(object sender, RoutedEventArgs e)
        {
            addConversation();
            goBack();
        }

        private void UITBGoBack_Click(object sender, RoutedEventArgs e)
        {
            goBack();
        }
        void goBack()
        {
            if (this.Frame.CanGoBack) this.Frame.GoBack();
            else
            {
                this.Frame.Navigate(typeof(AccountHomePage));
            }
        }
    }
}
