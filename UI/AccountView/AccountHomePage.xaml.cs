﻿using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using UniversalTCPChatClient.ObjectsHandeler;
using UniversalTCPChatClient.UI.ConversationView.conversationElements;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace UniversalTCPChatClient.UI.AccountView
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AccountHomePage : Page
    {

        private readonly CoreDispatcher dispatcher;
        private bool updatingConvUI = false;

        // timer
        private Timer tm;

        // data
        public ObservableCollection<ConversationElement> ConversationsList = new ObservableCollection<ConversationElement>();


        public string ConvID
        {
            get { return Core.convIDIn; }
            set { Core.convIDIn = value; }
        }

        public AccountHomePage()
        {
            this.InitializeComponent();
            this.dispatcher = CoreWindow.GetForCurrentThread().Dispatcher;
        }

        bool TimerCallback_ActionRuning = false;
        public void TimerCallBack(object state)
        {
            if (TimerCallback_ActionRuning == false)
            {
                TimerCallback_ActionRuning = true;
                System.Diagnostics.Debug.WriteLine("[ AccountHomePage ] : Timer callback");
                this.OnUiThread(async () =>
                {
                    await UpdateConversationListAsync();

                    // update selected conversation and display it if needed
                    if (await Core.Client.UpdateConversationAsync(ConvID))
                    {
                        DisplayConversation(ConvID);
                    }

                }).Wait();

                // timer update ending, allow the function to be called again
                TimerCallback_ActionRuning = false;
            }
        }

        /// <summary>
        /// Connect the connect to the server and get the covnersations
        /// </summary>
        public void CreateConnection()
        {
            if (Core.Client == null)
            {
                bool result = Core.CreateClient();
                System.Diagnostics.Debug.WriteLine("[ AccountHomePage ] : Connection result : " + result.ToString());
            }

            this.OnUiThread(async () =>
              {
                  await DisplayUI(); // display the UI
              }).Wait();

            // now that the connection with the server is created, we can start updating the conv list and the selected conv
            tm = new Timer(TimerCallBack, null, 10000, 10000);
        }


        public void ListConversations()
        {
            if (Core.Client != null)
            {
                List<ConversationElement> ids = Core.Client.GetConversationList();

                ConversationsList.Clear();

                if (ids.Count > 0)
                {
                    // if there is element in it

                    Stack_NoData.Visibility = Visibility.Collapsed;
                    foreach (ConversationElement elem in ids)
                    {
                        // automaticaly select the first conversation
                        if (ConvID == "")
                        {
                            ConvID = elem.ConvID;
                            UIConvName.Text = elem.ConvName;
                        }
                        ConversationsList.Add(elem);
                    }
                }
                else
                {
                    // there are no data, we should let them know it
                    Stack_NoData.Visibility = Visibility.Visible;
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Client not created");
            }
        }



        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            new Task(() => CreateConnection()).Start();
        }

        private async Task OnUiThread(Action action) { await this.dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => action()); }


        // UI

        bool refreshingUI = false;
        async Task UpdateConversationListAsync()
        {
            // indicate that we are starting updating conv list
            UIAccountProgress.IsActive = true;
            Stack_Loading.Visibility = Visibility.Visible;

            if (await Core.Client.UpdateConversationListAsync())
            {
                ListConversations();
            }

            // we have ended updating conv list
            UIAccountProgress.IsActive = false;
            Stack_Loading.Visibility = Visibility.Collapsed;
        }


        async Task DisplayUI()
        {

            if (refreshingUI == false && Core.Client != null)
            {
                refreshingUI = true;
                // indicate that we are loading the conversation list

                UIAccountProgress.IsActive = true;
                Stack_Loading.Visibility = Visibility.Visible;

                try
                {

                    System.Diagnostics.Debug.WriteLine("Account View : " + DateTime.Now.ToString() + " : Refreshing UI");


                    // first load from memory : 

                    ListConversations();

                    // now we have displayed something, we can turn off the progress indicator
                    // but we need to leave it read so the user now we have not finished updating the list

                    // display the conversation
                    if (ConvID != "") DisplayConversation(ConvID);

                    UITBUserName.Text = Core.AccountSelected.userName;


                }
                catch (Exception ex)
                {
                    Crashes.TrackError(ex);
                    UIGridUsername.Background = new SolidColorBrush(Colors.Red);
                    Stack_NoData.Visibility = Visibility.Visible;
                }



                // now we will try to download from the server 
                if (Core.Client.ClientIsConnected)
                {
                    try
                    {
                        // indicate that we can connect
                        Stack_CantConnect.Visibility = Visibility.Collapsed;
                        UIGridUsername.Background = new SolidColorBrush(Colors.Green);

                        if (await Core.Client.UpdateConversationListAsync())
                        {
                            ListConversations();
                        }


                        // update selected conversation and update display if necessary
                        if (await Core.Client.UpdateConversationAsync(ConvID))
                        {
                            DisplayConversation(ConvID);
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.Source = "AccountHomePage";

                        Crashes.TrackError(ex);
                        UIGridUsername.Background = new SolidColorBrush(Colors.Red);
                        Stack_CantConnect.Visibility = Visibility.Visible;
                    }
                }
                else if (Core.Client.ClientIsConnected == false)
                {
                    UIGridUsername.Background = new SolidColorBrush(Colors.Red);
                    Stack_CantConnect.Visibility = Visibility.Visible;
                    Analytics.TrackEvent("Client is not connected");
                }


                // indicate that we are no longer updating UI
                UIAccountProgress.IsActive = false;
                Stack_Loading.Visibility = Visibility.Collapsed;
                refreshingUI = false;
            }
        }

        private async void btUpdateConversationList_Click(object sender, RoutedEventArgs e)
        {
            await UpdateConversationListAsync();
        }

        private async void Conversations_ItemClick(object sender, ItemClickEventArgs e)
        {
            ConversationElement cv = (ConversationElement)e.ClickedItem;

            // update conv selected and name
            ConvID = cv.ConvID;
            UIConvName.Text = cv.ConvName;

            await DisplayAndUpdateConv(cv.ConvID);

        }



        /// <summary>
        /// Display the specified conversation, in case in which it has not been downloaded, it diplay nothing
        /// </summary>
        /// <param name="convID"></param>
        /// <returns></returns>
        private void DisplayConversation(string convID)
        {
            TCPConversation convIn = Core.Client.GetConversation(convID);
            // if the conversation doesn't exist, the conversation message list is empty    

            StackMessages.Children.Clear();
            foreach (var message in convIn.messages)
            {
                MessageView mv = new MessageView(message);
                StackMessages.Children.Add(mv);
            }

            // go to the end of the scrol view (aka : display the last message)
            svMessages.UpdateLayout();
            svMessages.ChangeView(0.0f, double.MaxValue, 1.0f);
        }


        /// <summary>
        /// Display the conversation and then update it from server and re display it
        /// </summary>
        /// <param name="convID"></param>
        /// <returns></returns>
        private async Task DisplayAndUpdateConv(string convID)
        {
            if (updatingConvUI == false)
            {

                // display the conversation from memory
                DisplayConversation(convID);

                bool result = await Core.Client.UpdateConversationAsync(convID);

                updatingConvUI = true;
                if (result)
                {
                    DisplayConversation(convID);
                }
                updatingConvUI = false;
            }

        }



        // messaging

        private void UIBtSend_Click(object sender, RoutedEventArgs e)
        {
            ObservableCollection<messageContent> messages = UIMessageComposer.GetMessage();
            foreach (var message in messages)
            {
                if (message.DataType == dataType.text)
                {
                    Core.Client.SendConversationMessage(message, ConvID);
                }
                else if (message.DataType == dataType.image)
                {
                    // send picture
                    // TODO : Implement the image sending mechanism
                }
            }

            UIMessageComposer.Reset();
        }

        private async void UITbAddFile_Click(object sender, RoutedEventArgs e)
        {

            FileOpenPicker fp = new FileOpenPicker();

            fp.FileTypeFilter.Add(".jpeg");
            fp.FileTypeFilter.Add(".png");
            fp.FileTypeFilter.Add(".bmp");
            fp.FileTypeFilter.Add(".jpg");

            StorageFile sf = await fp.PickSingleFileAsync();
            if (sf != null) UIMessageComposer.AddImage(sf);
        }

        private void AppBarConversationParameters_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Frame.Navigate(typeof(UI.ConversationView.ConversationSettingPage), null, new SlideNavigationTransitionInfo
                {
                    Effect = SlideNavigationTransitionEffect.FromRight
                });
            }
            catch (Exception ex)
            {
                this.Frame.Navigate(typeof(UI.ConversationView.ConversationSettingPage));
                Crashes.TrackError(ex);
            }
        }

        private async void BtCreateConversation_Click(object sender, RoutedEventArgs e)
        {
            await DisplayAndUpdateConv(ConvID);
        }

        private void BtUpdateConversation_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
