﻿using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace UniversalTCPChatClient.UI.AccountView
{
    public sealed partial class ConversationButton : Button
    {
        public string convID;
        public ConversationButton(string name, string convIDIn)
        {
            this.InitializeComponent();
            UITbName.Text = name;
            UITbReference.Text = convIDIn;
            this.convID = convIDIn;
        }
    }
}
