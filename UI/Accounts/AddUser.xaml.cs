﻿using Microsoft.AppCenter.Analytics;
using System;
using System.Collections.Generic;
using UniversalTCPClientLibrary;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace UniversalTCPChatClient.UI.Accounts
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AddUser : Page
    {
        public AddUser()
        {
            this.InitializeComponent();

            Analytics.TrackEvent("Navigated to adduser page");
        }


        // bt validate click
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (UITBAccountName.Text != "" && UITBHostname.Text != "" && UITBPort.Text != "" && UITBUsername.Text != "")
            {
                Account acc = new Account()
                {
                    userName = UITBUsername.Text,
                    accountName = UITBAccountName.Text,
                    host = UITBHostname.Text,
                    port = Int32.Parse(UITBPort.Text),
                    isWeb = UICbIsWeb.IsChecked.Value

                };
                acc.isWeb = UICbIsWeb.IsChecked.Value;
                if (acc.isWeb && acc.port == 6789) { acc.port = 8443; }

                String ID;
                do { ID = Guid.NewGuid().ToString("N"); }
                while (HasSameIDinList(ID));

                acc.ID = ID;

                Core.accountsList.Add(acc);
                Core.SaveAccountList();

                this.Frame.Navigate(typeof(AccountPage));
            }
        }

        bool HasSameIDinList(string ID)
        {
            foreach (Account accIter in Core.accountsList)
            {
                if (ID == accIter.ID) { return true; }
            }
            return false;
        }

        // bt cancel click
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            if (Frame.CanGoBack)
            {
                Frame.GoBack();
            }
        }
    }
}
