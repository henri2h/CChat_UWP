﻿using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using System;
using System.Collections.ObjectModel;
using UniversalTCPClientLibrary;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace UniversalTCPChatClient.UI
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AccountPage : Page
    {
        public ObservableCollection<Account> Accounts = new ObservableCollection<Account>();

        public AccountPage()
        {
            this.InitializeComponent();

            Analytics.TrackEvent("Navigated to account page");
            LoadAsync();
        }

        public async void LoadAsync()
        {
            Core.Client = null;
            try
            {
                await Core.LoadAccountList();
            }
            catch (Exception ex)
            {
                ex.Source = "AccountPage.load";
                Crashes.TrackError(ex);
            }

            LoadAccountsUI();

            foreach (var account in Core.accountsList)
            {
                if (account.IsDefault == true)
                {
                    Core.AccountSelected = account;

                    this.Frame.Navigate(typeof(UI.AccountView.AccountHomePage), account);
                }
            }
        }

        public void LoadAccountsUI()
        {
            Accounts.Clear();

            // list the accounts
            if (Core.accountsList.Count > 0) { Stack_NoUser.Visibility = Visibility.Collapsed; }
            else { Stack_NoUser.Visibility = Visibility.Visible; }

            foreach (Account selectedAccount in Core.accountsList)
            {
                Accounts.Add(selectedAccount);
            }
        }


        private void BtAdduser_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Frame.Navigate(typeof(UI.Accounts.AddUser), null, new SlideNavigationTransitionInfo
                {
                    Effect = SlideNavigationTransitionEffect.FromRight
                });
            }
            catch (Exception ex)
            {
                this.Frame.Navigate(typeof(UI.Accounts.AddUser)); // if slide navigation is not supported
                Crashes.TrackError(ex);
            }
        }
        // Get selected item and lauch navigation
        private void AccountList_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                Core.AccountSelected = (Account)e.ClickedItem;


                // set the default account which will be loaded next time the app is started
                foreach (var account in Core.accountsList)
                {
                    account.IsDefault = false;
                }

                Core.AccountSelected.IsDefault = true;
                Core.SaveAccountList();


                this.Frame.Navigate(typeof(UI.AccountView.AccountHomePage), (Account)e.ClickedItem, new DrillInNavigationTransitionInfo());
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
            }
        }

        private void AccountList_RightTapped(object sender, Windows.UI.Xaml.Input.RightTappedRoutedEventArgs e)
        {
        }

        private void BtGenerateCrashTest_Click(object sender, RoutedEventArgs e)
        {
            Crashes.GenerateTestCrash();
        }

        private void BtGenerateHandeledCrashTest_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Crashes.GenerateTestCrash();
            }
            catch (Exception ex)
            {
                Crashes.TrackError(ex);
            }
        }
    }
}
