﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using UniversalTCPChatClient.UI.ConversationView.conversationElements;
using UniversalTCPClientLibrary;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace UniversalTCPChatClient.UI.ConversationView
{
    public sealed partial class MessageComposer : UserControl
    {
        public MessageComposer()
        {
            this.InitializeComponent();
            AddTextBox();

        }

        public void DeleteItem()
        {
            UIListMessages.Children.Clear();
        }

        public void AddTextBox()
        {
            TextBox tb = new TextBox()
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                AcceptsReturn = true
            };
            UIListMessages.Children.Add(tb);
        }


        public ObservableCollection<messageContent> GetMessage()
        {
            ObservableCollection<messageContent> MessageContent = new ObservableCollection<messageContent>();

            foreach (UIElement elem in UIListMessages.Children)
            {

                messageContent mContent = new messageContent();

                if (elem is TextBox)
                {
                    TextBox tb = elem as TextBox;
                    mContent.DataType = dataType.text;
                    mContent.content = tb.Text;

                }
                else if (elem is ImageDisplay)
                {
                    ImageDisplay im = elem as ImageDisplay;
                    mContent.DataType = dataType.image;
                    mContent.content = im.FileName;
                    mContent.file = im.ContentFile;
                }
                else if (elem is FileDisplay)
                {
                    FileDisplay im = elem as FileDisplay;
                    mContent.DataType = dataType.file;
                    mContent.content = im.FileName;
                    mContent.file = im.ContentFile;
                }

                MessageContent.Add(mContent);
            }

            return MessageContent;
        }


        public async void AddImage(StorageFile sf)
        {
            ImageDisplay display = new ImageDisplay(); // we can give a null file name because this picture won't be displayed in the ImageViewer
            Task<bool> b = display.SetImage(sf);
            display.MaxHeight = 100;
            display.MaxWidth = 100;

            await b;

            UIListMessages.Children.Add(display);
            AddTextBox();
        }


        public void Reset()
        {
            UIListMessages.Children.Clear();
            AddTextBox();
        }

    }
}
