﻿  using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace UniversalTCPChatClient.UI.ConversationView
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ConversationSettingPage : Page
    {

        public String ConvName { get; set; }
        string ConvID { get { return Core.convIDIn; } }
        public ConversationSettingPage()
        {
            this.InitializeComponent();
        }

        async void LoadComponementsAsync()
        {
            UITbConvName.Text = await Core.Client.GetConversationInformationsAsync(ConvID);
        }

        private async void AppBarSave_ClickAsync(object sender, RoutedEventArgs e)
        {
            await Core.Client.SetConversationName(UITbConvName.Text, ConvID);
            GoBack();
        }

        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            GoBack();
        }

        void GoBack()
        {
            if (this.Frame != null && this.Frame.CanGoBack) this.Frame.GoBack();
        }
    }
}
