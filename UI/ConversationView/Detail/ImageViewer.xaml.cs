﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace UniversalTCPChatClient.UI.ConversationView.Detail
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ImageViewer : Page
    {
        public ImageViewer()
        {
            this.InitializeComponent();
        }

        public async Task LoadImage()
        {
            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/icons/appbar.image.png", UriKind.RelativeOrAbsolute));
            Task<bool> a = UIImage.SetImage(file);

            if (Core.selectedFileName != "")
            {
                UIImage.ConvID = Core.convIDIn;
                UIImage.FileName = Core.selectedFileName;
                await a;

                await UIImage.GetAndLoadFileAsync();
            }
        }

        public async void ForceImageDownload()
        {
            try
            {
                if (Core.selectedFileName != "")
                {
                    UIImage.ConvID = Core.convIDIn;
                    UIImage.FileName = Core.selectedFileName;
                    await UIImage.ForceGetAndDownloadFileAsync();
                }
            }
            catch
            {
                ContentDialog cantDown = new ContentDialog
                {
                    Title = "Download error",
                    Content = "Can't download image",
                    CloseButtonText = "Ok"
                };

                await cantDown.ShowAsync();
            }
        }

        private void AppBarGoBack_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                await LoadImage();
            }
            catch
            {
                var dialog = new MessageDialog("Could not download the image");
                await dialog.ShowAsync();

            }
        }

        private void AppBarRefresh_Click(object sender, RoutedEventArgs e)
        {
            ForceImageDownload();
        }
    }
}
