﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using UniversalTCPChatClient.UI.ConversationView.conversationElements;
using UniversalTCPClientLibrary;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;


namespace UniversalTCPChatClient.UI.ConversationView.conversationElements
{
    public sealed partial class MessageView : UserControl
    {
        public int maxWidth = 300;
        public const int maxHeight = 500;
        public TCPConversationMessage Message { get; internal set; }


        public MessageView()
        {
            this.InitializeComponent();
        }
        public MessageView(TCPConversationMessage message)
        {
            this.InitializeComponent();

            Message = message;
            // message is loaded in memory, display it :
            StartLoadingMessageAsync();
        }

        public async void StartLoadingMessageAsync()
        {
            await LoadMessageAsync();
        }

        public async Task<bool> LoadMessageAsync()
        {
            if (Message != null)
            {
                // display left if different username
                if (Message.Username != Core.AccountSelected.accountName)
                {
                    GRD.HorizontalAlignment = HorizontalAlignment.Left;
                }


                UIStackMessages.Children.Clear();
                if (Message.DataType == dataType.text)
                {
                    if (Message.Content != "")
                    {
                        TextBlock tb = new TextBlock()
                        {
                            Text = Message.Content,
                            MaxWidth = maxWidth
                        };
                        tb.TextWrapping = Windows.UI.Xaml.TextWrapping.WrapWholeWords;
                        UIStackMessages.Children.Add(tb);
                    }
                    else
                    {
                        GRD.HorizontalAlignment = HorizontalAlignment.Left;
                        TextBlock tb = new TextBlock()
                        {
                            Text = "#NOMESSAGE",
                            MaxWidth = maxWidth
                        };
                        tb.TextWrapping = Windows.UI.Xaml.TextWrapping.WrapWholeWords;
                        UIStackMessages.Children.Add(tb);
                    }
                }
                else if (Message.DataType == dataType.image)
                {
                    System.Diagnostics.Debug.WriteLine("Starting loading image");
                    Button bt = new Button();
                    bt.Click += new Windows.UI.Xaml.RoutedEventHandler(Core.DisplayImage);


                    ImageDisplay ds = new ImageDisplay(Message.Content);
                    ds.LoadingFileFromConversation = true;
                    ds.MaxHeight = maxHeight;
                    ds.MaxWidth = maxWidth;

                    if (Core.Client.FileAlreadyDownloaded(Message.Content, Message.ConvID))
                    {
                        await ds.SetImage(await Core.Client.GetFileAsync(Message.Content, Message.ConvID));
                    }
                    else
                    {
                        StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/icons/appbar.image.png", UriKind.RelativeOrAbsolute));

                        await ds.SetImage(file);

                        // download and display files
                        await ds.SetImage(await Core.Client.GetFileAsync(Message.Content, Message.ConvID));
                    }
                    ds.LoadingFileFromConversation = true;
                    /*
                    ds.ConvID = Message.ConvID;
                    ds.FileName = Message.Content;
                    ds.RegisterToDownload(Message.Content, Message.ConvID);
                    */

                    System.Diagnostics.Debug.WriteLine("Loading image terminated");
                    bt.Content = ds;
                    UIStackMessages.Children.Add(bt);
                }
                else if (Message.DataType == dataType.file)
                {
                    Button bt = new Button();
                    FileDisplay ds = new FileDisplay();

                    ds.MaxHeight = maxHeight;
                    ds.MaxWidth = maxWidth;

                    ds.ConvID = Message.ConvID;
                    ds.FileName = Message.Content;
                    ds.RegisterToDownload(Message.Content, Message.ConvID);

                    // Load file
                    bt.Content = ds;

                    UIStackMessages.Children.Add(bt);
                }
                else
                {
                    TextBlock tb = new TextBlock()
                    {
                        MaxWidth = maxWidth,
                        Text = "No data type set : " + Message.Content
                    };
                    UIStackMessages.Children.Add(tb);
                }

                return true;
            }
            else
            {
                // we can't load the message yet
                System.Diagnostics.Debug.WriteLine("Could not message yet");
                return false;
            }

        }

        public void ChangeGridBackgroundColor(Color c)
        {
            GRD.Background = new SolidColorBrush(c);
        }
    }

}
