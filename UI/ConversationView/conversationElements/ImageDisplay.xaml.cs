﻿using System;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Storage.Streams;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Imaging;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace UniversalTCPChatClient.UI.ConversationView.conversationElements
{
    public sealed partial class ImageDisplay
    {
        public string FileName { get; set; }
        public string ConvID { get; set; }

        public byte[] ContentFile { get { return PrivateContentFile; } }

        byte[] PrivateContentFile;

        public bool LoadingFileFromConversation
        {
            get { return LoadFileFromConversation; }
            set
            {
                LoadFileFromConversation = value;
                if (value)
                {
                    UIProgressRing.Visibility = Visibility.Visible;
                    UIProgressRing.IsActive = true;
                }
                else
                {
                    UIProgressRing.Visibility = Visibility.Collapsed;
                    UIProgressRing.IsActive = false;
                }
            }
        }

        private bool LoadFileFromConversation = false;

        public ImageDisplay(string fileName)
        {
            this.FileName = fileName;
            this.InitializeComponent();
        }

        /// <summary>
        /// warning, not setting the FileName element can cause issues ...
        /// </summary>
        public ImageDisplay()
        {
            this.InitializeComponent();
        }

        public async Task<bool> SetImage(StorageFile sf)
        {
            FileName = Path.GetFileName(sf.Path);
            this.PrivateContentFile = await ReadFile(sf);
            return await SetImage(PrivateContentFile);
        }

        public async Task<bool> SetImage(byte[] imageBytes)
        {
            BitmapImage BImg = await ImageFromBytes(imageBytes);
            return SetImage(BImg);
        }

        public bool SetImage(BitmapImage bmp)
        {
            UIImage.Source = bmp;
            System.Diagnostics.Debug.WriteLine("Image loaded");
            return true;
        }

        internal async Task<bool> GetAndLoadFileAsync()
        {
            byte[] items = await Core.Client.GetFileAsync(FileName, ConvID, 1);
            return await SetImage(items);
        }

        internal async Task<bool> ForceGetAndDownloadFileAsync()
        {
            byte[] items = await Core.Client.GetFileAsync(FileName, ConvID, 2);
            return await SetImage(items);
        }

        public async Task<byte[]> ReadFile(StorageFile file)
        {
            byte[] fileBytes = null;
            using (IRandomAccessStreamWithContentType stream = await file.OpenReadAsync())
            {
                fileBytes = new byte[stream.Size];
                using (DataReader reader = new DataReader(stream))
                {
                    uint x = await reader.LoadAsync((uint)stream.Size);
                    System.Diagnostics.Debug.WriteLine(x.ToString());
                    reader.ReadBytes(fileBytes);
                }
            }
            return fileBytes;
        }


        public async static Task<BitmapImage> ImageFromBytes(Byte[] bytes)
        {
            BitmapImage image = new BitmapImage();
            using (InMemoryRandomAccessStream stream = new InMemoryRandomAccessStream())
            {
                await stream.WriteAsync(bytes.AsBuffer());
                stream.Seek(0);
                image.SetSource(stream);
            }
            return image;
        }


    }
}
